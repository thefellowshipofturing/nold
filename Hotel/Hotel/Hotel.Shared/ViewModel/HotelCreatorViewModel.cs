﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml.Controls;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Hotel.Common;
using Hotel.Models;
using RelayCommand = GalaSoft.MvvmLight.Command.RelayCommand;

namespace Hotel.ViewModel
{
    public class HotelCreatorViewModel : ViewModelBase
    {

        public RelayCommand PickFile { get; set; }
        public RelayCommand SaveAndClose { get; set; }
        public Models.Hotel Hotel { get; set; }
        public ObservableCollection<City> Cities { get; set; }
        public ObservableCollection<Feature> Features { get; set; }

        public HotelCreatorViewModel()
        {
            PickFile = new RelayCommand(PickFileAction);
            SaveAndClose = new RelayCommand(SaveAndCloseAction);
            Hotel = new Models.Hotel();
            Cities = SimpleIoc.Default.GetInstance<CityService>().Cities;
            Features = SimpleIoc.Default.GetInstance<FeatureService>().Features;
        }

        private void SaveAndCloseAction()
        {
            
        }

        private async void PickFileAction()
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".jpeg");
            openPicker.FileTypeFilter.Add(".png");

            StorageFile file = await openPicker.PickSingleFileAsync();
            if (file != null)
            {
               
            }

        }

    }

    public class CityService
    {
        public ObservableCollection<City> Cities
        {
            get
            {
                return new ObservableCollection<City>
                {
                    new City("Oslo"),
                    new City("Amsterdam"),
                    new City("Warszawa"),
                    new City("Krakow"),
                    new City("Londyn"),
                    new City("Berlin"),
                    new City("Bonn"),
                    new City("Paryz"),
                    new City("Barcelona"),
                    new City("Kijow")
                };
            }
        }
    }
}
