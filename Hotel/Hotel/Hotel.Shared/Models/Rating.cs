﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Hotel.Models
{
    public class Rating
    {
        public String Comment { get; set; }
        public String User { get; set; }
        public float Stars { get; set; }
    }
}
