﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Hotel.Models;

namespace Hotel
{
    class FeatureService
    {
        public ObservableCollection<Feature> Features
        {
            get
            {
                return new ObservableCollection<Feature>
                {
                    new Feature("Wi-Fi", "Darmowe"),
                    new Feature("TV", "Cyfrowy Polsat")

                };
            }
        }
    }
}
