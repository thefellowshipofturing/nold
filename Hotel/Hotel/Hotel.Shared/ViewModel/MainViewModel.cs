using System;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Hotel.Models;

namespace Hotel.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private string _hotelName;
        private string _cityName;

        public ObservableCollection<Models.Hotel> Hotels { get; set; }
        public ObservableCollection<City> Cities { get; set; }
        public ObservableCollection<Rating> Ratings { get; set; }

        public ObservableCollection<Feature> FilterFeatures { get; set; }

        public String HotelName
        {
            get { return _hotelName;  }
            set
            {
                _hotelName = value;
                RaisePropertyChanged();
            }
        }

        public String CityName
        {
            get { return _cityName; }
            set
            {
                _cityName = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Models.Hotel> FilteredHotels { get; set; }
        public RelayCommand Filter { get; set; }



        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            Hotels = new ObservableCollection<Models.Hotel>();
            Cities = new ObservableCollection<City>();
            Ratings = new ObservableCollection<Rating>();

            FilteredHotels = new ObservableCollection<Models.Hotel>();

            Filter = new RelayCommand(() =>
            {
                FilteredHotels.Clear();

                foreach (var hotel in Hotels)
                {
                    if (string.IsNullOrEmpty(CityName))
                    {
                        if (FilterFeatures.All(feature => hotel.Features.Contains(feature)))
                        {
                            FilteredHotels.Add(hotel);
                        }
                    }
                    else if(hotel.City.Name.StartsWith(CityName))
                    {
                        if (FilterFeatures.All(feature => hotel.Features.Contains(feature)))
                        {
                            FilteredHotels.Add(hotel);
                        }
                    }
                }
            });

            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
        }

    }
}