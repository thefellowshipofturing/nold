﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Windows.UI.Xaml.Controls;

namespace Hotel.Models
{
    public class City : INotifyPropertyChanged
    {
        private Image _photo;
        public String Name { get; set; }
        public ObservableCollection<Hotel> Hotels { get; set; }

        public Image Photo
        {
            get { return _photo; }
            set
            {
                if (Equals(value, _photo)) return;
                _photo = value;
                OnPropertyChanged();
            }
        }

        public City(String name)
        {
            Hotels = new ObservableCollection<Hotel>();
            Name = name;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
