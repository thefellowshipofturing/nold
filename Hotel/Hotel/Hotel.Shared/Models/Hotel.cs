﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Windows.UI.Xaml.Controls;
//using Hotel.Annotations;

namespace Hotel.Models
{
    public class Hotel : INotifyPropertyChanged
    {
        public ObservableCollection<Rating> Ratings { get; set; }
        public ObservableCollection<Feature> Features { get; set; }
        public City City { get; set; }

        private string _name;
        private Image _image;

        public String Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        public Image Image
        {
            get { return _image; }
            set
            {
                if (Equals(value, _image)) return;
                _image = value;
                OnPropertyChanged();
            }
        }

        public float Stars
        {
            get { return Ratings.Average(r => r.Stars); }
        }

        public Hotel()
        {
            Ratings = new ObservableCollection<Rating>();
            Features = new ObservableCollection<Feature>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
